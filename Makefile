CC = gcc -g --std=c89

default: test

test: test.c lib/libcact.a
	${CC} $^ -o $@

lib/libcact.a: bin/cact.o
	ar -rcv $@ $^

bin/cact.o: src/cact.c src/cact.h
	${CC} -c src/cact.c -o $@

clean:
	@echo removing bin/* and lib/*
	rm bin/* lib/*
