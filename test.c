#include <stdio.h>
#include <stdlib.h>
#include "src/cact.h"

int
main()
{
	if (cact_initvars())
	{
		fprintf(stderr, "error: cact_initvars() returned non-zero code");
		return 2;
	}

	printf("Using alphabet %s and ciphertext %s\n", cact_alphabet, cact_ciphertext);

	unsigned int b;
	char *plaintext;
	for (b = 0; b <= 25; b++) {
		cact_decipher_affine(1, b);
		printf("%s\n", plaintext);
	}

	cact_freevars();

	return 0;
}
