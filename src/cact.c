#include "cact.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

unsigned int
cact_initvars()
{
	if (cact_alphabet_set("ABCDEFGHIJKLMNOPQRSTUVWXYZ")) {
		fprintf(stderr, "error: unable to set alphabet - most likely allocation error\n");
		return 2;
	}
	if (cact_ciphertext_set("JGNNQ")) {
		fprintf(stderr, "error: unable to set ciphertext - most likely allocation error\n");
		return 2;
	}
	return 0;
}

void
cact_freevars()
{
	free(cact_alphabet);
	free(cact_ciphertext);
	free(cact_plaintext);
}

enum CACT_RETURNCODE
cact_alphabet_set(char *abc)
{
	cact_alphabet = abc;
	if(!cact_alphabet)
			return CACT_ERROR_ALPHABET_SET;
	else
			return CACT_SUCCESS;
}

enum CACT_RETURNCODE
cact_ciphertext_set(char *ct)
{
	if (cact_ciphertext)
		free(cact_ciphertext);

	cact_ciphertext = ct;
	if (!cact_ciphertext)
		return CACT_ERROR_CIPHERTEXT_SET;
	else
		return CACT_SUCCESS;
}

enum CACT_RETURNCODE
cact_decipher_affine(unsigned int a, unsigned int b)
{
	/* clear the plaintext */
	if (cact_plaintext)
		free(cact_plaintext);

	unsigned int bufsize = CACT_TEXT_BUFSIZE;
	cact_plaintext_num = malloc( sizeof(int) * CACT_TEXT_BUFSIZE );

	if (!cact_plaintext_num) {
		fprintf(stderr, "cact: in cact_decipher_affine() - allocation error\n");
		return CACT_ERROR_DECIPHER_AFFINE_PLAINTEXT_NUM_ALLOCATION;	
	}

	/*
	walk through the ciphertext;
		for each character in the ciphertext, walk through the alphabet until you find a matching character - if you don't, then exit with an error
		for each number in the ciphertext_num array, apply the decryption function to it
		for each number in the plaintext_num array, add a character to the plaintext array corresponding to the character in the alphabet
	*/
	unsigned int i = 0;
	while (cact_ciphertext[i] != '\0')
	{
	
		unsigned int *cact_ciphertext_num = 
			malloc( sizeof(cact_ciphertext)/sizeof(char) * sizeof(unsigned int) );

		if (!cact_ciphertext_num) {
			fprintf(stderr, "cact: in cact_decipher_affine() - allocation error\n");
			return CACT_ERROR_DECIPHER_AFFINE_CIPHERTEXT_NUM_ALLOCATION;
		}

		if (i >= bufsize) {
			bufsize += CACT_TEXT_BUFSIZE;
			cact_plaintext_num = realloc(cact_plaintext_num, bufsize);
		}

		/* CONVERSION */
		for (cact_ciphertext_num[i] = 0; cact_alphabet[cact_ciphertext_num[i]] != cact_ciphertext[i]; (cact_ciphertext_num[i])++) {
			if (cact_alphabet[cact_ciphertext_num[i]] == '\0') {
				fprintf(stderr, "error: could not find corresponding character for ciphertext in alphabet\n");
				return CACT_ERROR_DECIPHER_AFFINE_CONVERSION;
			}
		}

		/* DECRYPTION */
		cact_plaintext_num[i] = ((cact_ciphertext_num[i] - b + 26)/a ) % strlen(cact_alphabet); 

		/* DECONVERSION */
		cact_plaintext = malloc( sizeof(cact_plaintext_num)/sizeof(int) * sizeof(char) );
		if (!cact_plaintext)
			return CACT_ERROR_DECIPHER_AFFINE_PLAINTEXT_ALLOCATION;

		cact_plaintext[i] = cact_alphabet[cact_plaintext_num[i]];
		printf("%c", cact_plaintext[i] );

		i++;
		
	}

	cact_plaintext[i] = '\0';

	return CACT_SUCCESS;
}
