#define CACT_TEXT_BUFSIZE 16
#define CACT_LINE_BUFSIZE 16

/* GLOBAL VARIABLES */
char *cact_alphabet; /* a = 0, b = 1, c = 2, ..., y = 24, z = 25 */
char *cact_ciphertext;
unsigned int *cact_ciphertext_num;
char *cact_plaintext;
unsigned int *cact_plaintext_num;

/*ERRORS*/
enum CACT_RETURNCODE
{
	CACT_SUCCESS,
	CACT_ERROR_ALPHABET_SET,
	CACT_ERROR_CIPHERTEXT_SET,
	CACT_ERROR_DECIPHER_AFFINE_PLAINTEXT_NUM_ALLOCATION,
	CACT_ERROR_DECIPHER_AFFINE_CIPHERTEXT_NUM_ALLOCATION,
	CACT_ERROR_DECIPHER_AFFINE_PLAINTEXT_ALLOCATION,
	CACT_ERROR_DECIPHER_AFFINE_CONVERSION
};

/* GLOBAL FUNCTIONS - VARIABLE MANAGEMENT */
unsigned int cact_initvars();
void cact_freevars();
enum CACT_RETURNCODE cact_alphabet_set(char *abc);
enum CACT_RETURNCODE cact_ciphertext_set(char *ct);

/* GLOBAL FUNCTIONS - SPECIFIC CIPHER DECRYPTION FUNCTIONS */
enum CACT_RETURNCODE cact_decipher_affine(unsigned int a, unsigned int b);
