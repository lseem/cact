# cact

Cryptography and Cipher Tools

A library for cracking ciphers.

## constituents

* cact :  the library which contains various cracking functions
* cacsh:	a basic shell used to implement the functions

## goals

To create shell tools to aid in the cracking of unknown ciphers.
